
*读入数据
infix V1 1-3       V2 4-5       V3 6-6          ///
      V4 7-7       V5 8-8       V6 9-9          ///  
      V7 10-10     V8 11-11     V9 12-12        ///  
      V10 13-13    V11 14-14    V12 15-15       ///
      V13 16-16    V14 17-17    V15 18-18       ///
      V16 19-19    V17 20-20    V18 21-21       ///
      V19 22-22    V20 23-23    V21 24-24       ///
      V22 25-25    V23 26-26    V24 27-27       ///
      V25 28-29    V26 30-31    V27 32-33       ///
      V28 34-35    V29 36-37    V30 38-39       ///
      V31 40-41    V32 42-43    V33 44-44       ///
      V34 45-46    V35 47-48    V36 49-49       ///
      V37 50-50    V38 51-51    V39 52-52       ///
      V40 53-53    V41 54-54    V42 55-55       ///
      V43 56-56    V44 57-57    V45 58-58       ///
      V46 59-59    V47 60-60    V48 61-61       ///
      V49 62-62    V50 63-63    V51 64-64       ///
      V52 65-65    V53 66-66    V54 67-67       ///
      V55 68-69    V56 70-70    V57 71-71       ///
      V58 72-73    V59 74-74    V60 75-75       ///
      V61 76-76    V62 77-78    V63 79-80       ///
      V64 81-82    V65 83-83    V66 84-84       ///
      V67 85-86    V68 87-87    V69 88-89       ///
      V70 90-90    V71 91-91    V72 92-92       ///
      V73 93-93    V74 94-94    V75 95-95       ///
      V76 96-96    V77 97-97    V78 98-98       ///
using 07273-0001-Data.txt, clear

rename *, lower        //把所有变量名变小写

*Give variables some descriptive labels
*Communist danger; 对于 Communist 的恐惧程度
generate cdanger = .
replace cdanger = 0 if v7==5 | v7==4 | v7==3
replace cdanger = 1 if v7==1 | v7==2
label define cdanger 0 "some/little danger" 1 "very great/great danger"
label variable cdanger "Communist danger at present time?"    //数值越高代表恐惧程度越高
label values cdanger cdanger

*Letter-writing
generate letter = .
replace letter = 0 if v13==4
replace letter = 1 if v13==2
label define noyes 0 "no" 1 "yes"
label variable letter "Written to politician in last year?"   //过于一年是否有给政客写信
label values letter noyes

*Partisan affiliation
generate party = .
replace party = 0 if v9==4
replace party = 1 if v9==2
replace party = 2 if v9==6 | party==8
label define party 0 "dem" 1 "rep" 2 "other"   //0 民主党； 1 共和党；2 其他
label variable party "partisan affiliation"    //政治面貌
label values party party

*Fight Russia?
generate russia = .
replace russia = 0 if v8==1 | v8==2 | v8==4 | v8==8
replace russia = 1 if v8==6
label variable russia "Fight Russia?"
label values russia noyes

*Attend political meetings?
generate meeting = .
replace meeting = 0 if v16==4
replace meeting = 1 if v16==2
label variable meeting "Attend political meetings?"   //是否参与过政治集会
label values meeting noyes

*Belong to political organization?
generate org = .
replace org = 0 if v18==4
replace org = 1 if v18==2
label variable org "Belong to political org?"  //是否附属于某个政治组织
label values org noyes

*Public officials care about your opinion?
generate popinion = .
replace popinion = 0 if v36==2
replace popinion = 1 if v36==4
label define popinion 0 "don't care" 1 "care"
label variable popinion "Public officials care about your op?" //你认为公务员是否在乎你的意见
label values popinion popinion

*Sould Socialist P publish in US during peacetime?
generate snews = .
replace snews = 0 if v37==2
replace snews = 1 if v37==4
label variable snews "Should Socialist P publish in US during peacetime?"
label values snews noyes

*Do Communist professors in US have much influence?
generate profess = .
replace profess = 0 if v38==2
replace profess = 1 if v38==4
label variable profess "Do Communist professors in US have much influence?"  //你认为相信共产主义的教授在美国有市场吗？
label values profess noyes

*Do Communists have a lot of influence in the D Party?
generate cdems = .
replace cdems = 0 if v40==2
replace cdems = 1 if v40==4
label variable cdems "Do Communists have a lot of influence in the D Party?" //你认为共产主义者对民主党是否有很大影响
label values cdems noyes

*Free speech for Communists on US radio/TV?
generate cspeech = .
replace cspeech = 0 if v42==4
replace cspeech = 1 if v42==2
label variable cspeech "Free speech for Communists on US radio/TV?"
label values cspeech noyes

*Communists in my your neighborhood?
generate cneighbor = .
replace cneighbor = 0 if v43==4
replace cneighbor = 1 if v43==2
label variable cneighbor "Communists in my your neighborhood?"
label values cneighbor noyes

*Local communists more dangerous than Russian or China?
generate clocal = .
replace clocal = 0 if v50==2
replace clocal = 1 if v50==4
label variable clocal "Local communists more dangerous than Russian or China?"
label values clocal noyes

*Sex
generate sex = .
replace sex = 0 if v54==2
replace sex = 1 if v54==4
label define sex 0 "male" 1 "female"
label variable sex "sex of R"
label values sex sex

*Age
generate age=v55
recode age (00=10)(10=11)(99=.)
label define age 01 "20-24" 11 "70+"
label variable age "age of R"
label values age age

*Schooling											
generate school=v56
recode school (1=1)(2=1)(3=2)(4=2)(5=3)(6=3)(else=.)
label define school 1 "some/complete grammar school" 2 "some/complete HS" ///
	3 "some/complete college"
label variable school "How much schooling did you complete?"
label values school school

*Union member?										
generate union = .
replace union = 0 if v61==4
replace union = 1 if v61==2
label variable union "Union member?"
label values union noyes

*Religion										
generate relig = .
replace relig = 0 if v68==0
replace relig = 1 if v68==2
replace relig = 2 if v68==4
replace relig = 3 if v68==6
replace relig = 4 if v68==8
label define relig 0 "none" 1 "protestant" 2 "catholic" 3 "jewish" 4 "other"
label variable relig "R's religion"
label values relig relig

*Income											
generate income=v72
recode income (2=1)(4=2)(6=3)(8=4)(0=5)(else=.)
label define income 1 "under $4,000" 5 "$15,000 and over"
label variable income "Total fam income in previous year bef taxes"
label values income income

*Industry											
generate industry=v58
recode industry (1=1)(2=2)(3=2)(4=2)(5=2)(6=2)(7=3)(8=3)(9=3)(0=4)(else=.)
label define industry 1 "agriculture" 2 "business" 3 "servivice" 4 "government"
label variable industry "head of house industry"
label values industry industry

*Influence vote?									
generate influence = .
replace influence = 0 if v14==4
replace influence = 1 if v14==2
label variable influence "Do you try to influence vote?"
label values influence noyes

*Give money/buy tickets?                         
generate money = .
replace money = 0 if v15==4
replace money = 1 if v15==2
label variable money "Do you give money/buy tickets for campaigns?"
label values money noyes

*Do work for party/candidate?                     
generate work = .
replace work = 0 if v17==4
replace work = 1 if v17==2
label variable work "Do you do any other work for party/cand?"
label values work noyes

*Employer
generate selfemp = .
replace selfemp = 0 if v57==4 | v57==6 | v57==8
replace selfemp = 1 if v57==2
label define selfemp 0 "not self-employed" 1 "self-employed"
label variable selfemp "Is R's head of house self-employed?"
label values selfemp selfemp
	
sum cdanger - selfemp
save rright.dta, replace