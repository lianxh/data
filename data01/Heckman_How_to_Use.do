
*------------------------------选择模型-----------------------------------

*--------------样本选择模型-------------
use womenwk.dta, clear

*ols
reg wage educ age
est store OLS

*heckman maximum likelihood
heckman wage educ age, select(married children educ age) //默认最大似然估计
est store HeckMLE

*heckman two-step  all-in-one 不可以进行cluster调整
heckman wage educ age, select(married children educ age) twostep
est store Heck2s

*heckman two-step  step-by-step 可以进行cluster调整
probit work married children educ age
est store First
predict y_hat, xb
gen pdf = normalden(y_hat)  //概率密度函数
gen cdf = normal(y_hat)     //累积分布函数
gen imr = pdf/cdf           //计算逆米尔斯比率
reg  wage educ age imr if work == 1  //女性工作子样本
est store Second
vif  //方差膨胀因子

*对比结果
local m "OLS HeckMLE Heck2s First Second"
esttab `m', mtitle(`m') nogap compress pr2 ar2

*--------------处理效应模型-------------
use union3.dta, clear

*ols
reg wage age grade smsa black tenure union
est store OLS

*etregress maximum likelihood
etregress wage age grade smsa black tenure,  ///
	  treat(union = south black tenure) twostep 
est store etrMLE

*etregress two-step all-in-one  不可以进行cluster调整
etregress wage age grade smsa black tenure,  ///
		  treat(union = south black tenure) twostep 
est store etr2s	

*etregress two-step  step-by-step 可以进行cluster调整
probit union south black tenure
est store First
predict y_hat, xb
gen pdf = normalden(y_hat)
gen cdf = normal(y_hat)
gen imr1 = pdf/cdf            //union = 1算法
gen imr2 = (-pdf) / (1 - cdf) //union = 0算法
gen imr = imr1 
replace imr = imr2 if union == 0
reg wage age grade smsa black tenure union imr
est store Second
vif

*对比结果
local m "OLS etrMLE etr2s First Second"
esttab `m', mtitle(`m') nogap compress pr2 ar2
