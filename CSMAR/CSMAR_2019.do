
*    
*     
*       ██████ ▄▄▄█████▓ ▄▄▄     ▄▄▄█████▓ ▄▄▄      
*     ▒██    ▒ ▓  ██▒ ▓▒▒████▄   ▓  ██▒ ▓▒▒████▄    
*     ░ ▓██▄   ▒ ▓██░ ▒░▒██  ▀█▄ ▒ ▓██░ ▒░▒██  ▀█▄  
*       ▒   ██▒░ ▓██▓ ░ ░██▄▄▄▄██░ ▓██▓ ░ ░██▄▄▄▄██ 
*     ▒██████▒▒  ▒██▒ ░  ▓█   ▓██▒ ▒██▒ ░  ▓█   ▓██▒
*     ▒ ▒▓▒ ▒ ░  ▒ ░░    ▒▒   ▓▒█░ ▒ ░░    ▒▒   ▓▒█░
*     ░ ░▒  ░ ░    ░      ▒   ▒▒ ░   ░      ▒   ▒▒ ░
*     ░  ░  ░    ░        ░   ▒    ░        ░   ▒   
*           ░                 ░  ░              ░  ░
*                                                                  
*     
*                                                       
*       _________________________________________       
*       —————————————————————————————————————————       
*          1990-2019 CSMAR中国上市公司数据处理               
*                        北京                           
*                    (2020.3.19)                        
*       _________________________________________       
*       ————————————————————————————————————————— 

*====================
*     -基本信息-
*====================

    *-样本区间：1990-2019年
	*-基本资料：中国上市数据库
    *-数据来源：CSMAR（国泰安数据库） http://www.gtarsc.com/

*-注意：执行后续命令之前，请先执行如下命令
 *global path "D:\Data\CSMAR" 
  global path "D:\DATA\GTA2018-CSMAR"
  global D    "$path\CSMAR_csv"          
  global Out  "$path\out"       
  global do   "$path\label_do"
  adopath +   "$path\adofiles"  
  cd "$D" 
  
*====================
*     -注意事项-
*====================

*-从 CSMAR 下载每个子库的数据，选择全部字段和年度；保存为 csv 创建stata格式
* 另存时，文件名最好为 CSMAR 库中的子库中文名称，以便能够区分


*================================================================
*-基本资料: 注册地, 上市时间等
*-数据来源: [股票市场系列]-[CSMAR中国股票市场交易数据库]-[基本数据]
*================================================================
    local ff "TRD_Co"
    import delimited "`ff'.csv", case(preserve) encoding(UTF-16LE) clear 
	
	*destring stkcd crcd indcd commnt, replace //字符->数字
	*-Note：csv格式数据优势，导入就是数字格式，并不需要执行上述代码
	
	foreach v in Estb List Ipo Stat{           //批量转化时间格式
	  renvars `v'dt, suffix(_tmp)
	  gen `v'dt = date(`v'dt_tmp, "YMD")
	  format `v'dt %td
	  gen `v'year = year(`v'dt)  
	}
	*drop if listyear>2016 		//剔除上市不满3年的公司（可选）
	rename Statco Statco_tmp    
	encode Statco_tmp, gen(Statco)
	label drop Statco
	drop *_tmp 					//Statco公司活动情况 转数值型
	*-变量标签                  //见DES代码说明文件 .txt
	label var Cuntrycd 国家代码
	label var Stkcd 证券代码
	label var Stknme 证券简称
	label var Conme 公司全称
	label var Conme_en 公司英文全称
	label var Indcd 行业代码A
	label var Indnme 行业名称A
	label var Nindcd 行业代码B
	label var Nindnme 行业名称B
	label var Nnindcd 行业代码C
	label var Nnindnme 行业名称C
	label var Estbdt 公司成立日期
	label var Listdt 上市日期
	label var Favaldt 数据库最早交易记录的日期
	label var Curtrd 数据库中交易数据的计量货币
	label var Ipoprm 股票发行溢价
	label var Ipoprc 发行价格
	label var Ipocur 发行价格的计量货币
	label var Nshripo 发行数量
	label var Parvcur 股票面值的计量货币
	label var Ipodt 发行日期
	label var Parval 股票面值
	label var Sctcd 区域码
	label var Statco 公司活动情况
	label var Crcd AB股交叉码
	label var Statdt 情况变动日
	label var Commnt H股交叉码
	label var Markettype 市场类型
	
	label var Estbyear 公司成立年份
	label var Listyear 公司上市年份
	label var Ipoyear  发行年份
	label var Statyear 情况变动年份
	
	drop Cuntrycd Conme Conme_en Ipocur Parvcur Favaldt Statdt   
	
	*添加数字-文字对应表
	  label define Markettype   1 "上海A" 2 "上海B" 4 "深圳A" 8 "深圳B" 16 "创业板" 
	  label value Markettype markettype 
	  label define Indcd   1 "金融" 2 "公用事业" 3 "房地产" 4 "综合" 5 "工业" 6 "商业"  
      label value Indcd  Indcd   
      label define Statco   1 "正常交易" 2 "终止上市" 4 "暂停上市"  3 "停牌"  
      label value Statco  Statco  
    *-变量顺序
	  order Stkcd Stknme *year *ind* *Stat* *Ipo* 
    *-加变量前缀
	  *renvars _all, prefix(bc_)
	  *rename (bc_stkcd bc_stknme) (stkcd stknme)
 
  *-定义一些新变量
	*-成立和上市年份
	  gen Estbyr = year(Estbdt)
	  label var Estbyr "成立年份"
	  gen Listyr = year(Listdt)
	  label var Listyr "上市年份"
	*-仅发行 A 股标示
	  gen Aonly = (Crcd==.)&(Commnt==.)  
	  label var Aonly "是否仅发行A股"
	  label define Aonly 1 "是" 0 "否"
	  label value  Aonly Aonly
	  
    save "$Out\TRD_Co.dta", replace   //公司基本信息资料汇总


*================================================================
*-基本资料: 行业代码 (证监会2012行业分类码，逐年更新)
*-数据来源:[中国上市公司财务指标分析数据库/偿债能力]
*================================================================
    local ff "FI_T1"
    import delimited "`ff'.csv", case(preserve) encoding(UTF-16LE) clear 
	keep if Typrep=="A"  				 //仅保留合并报表
	gen year = real(substr(Accper,1,4))
    *destring stkcd, replace			 //csv格式导入，无需转换
	gen str2 month = substr(Accper,6,7)	 //产生月份
    keep if month == "12"           //仅保留年度数据（如果年度导入无需此操作）
	xtset Stkcd year
	rename Indcd Sicda_str
	encode Sicda_str, gen(Sicda)	
	bysort Stkcd: egen sd_Sicda = sd(Sicda)
	
	label var Sicda     "证监会2012行业大类分类码,每年更新"
	label var Sicda_str "证监会2012行业大类分类码,每年更新"

	gen Sicmen_str = substr(Sicda_str,1,1)
	encode Sicmen_str, gen(Sicmen)	
	bysort Stkcd: egen sd_Sicmen = sd(Sicmen)	
    gen Sicmen_chg = (sd_Sicmen!=0)
	label var Sicmen_chg "行业门类代码是否变更"
	label define Sicmen_chg 1 "行业变更" 0 "行业未变更"
	label value  Sicmen_chg Sicmen_chg
	
   *-行业划分(制造业细分到次类，其他行业采用门类)
   *-参见 黄梅,夏新平(2009)  南开管理评论
     clonevar Sic2_str = Sicda_str
     replace Sic2_str = substr(Sic2,1,1) if substr(Sic2,1,1)!="C"
     replace Sic2_str = substr(Sic2,1,2) if substr(Sic2,1,1)=="C"
	 encode Sic2_str, gen(Sic2) label(Sic2)
	 label var Sic2     "行业分类: A B C1 C2 D E"
	 label var Sic2_str "行业分类: A B C1 C2 D E"
	 *replace sic2 = "C9" if sic2=="C2"  // 将 C2 并入 C9
   *-每年度每个行业至少保留 15 家公司
   *  bysort sic2 year: gen num_sic_year = _N
   *  keep if num_sic_year >=15  // Roychowdhury(2006, p.349) 
   *-基本统计分析
   *  tab sic2 year	
   keep  Stkcd year Typrep Sic2 Sic2_str ///
				Sicda_str Sicda Sicmen Sicmen_str Sicmen_chg
   order Stkcd year Typrep Sic2 Sic2_str ///
				Sicda_str Sicda Sicmen Sicmen_str Sicmen_chg
   save "$Out\Indus.dta", replace
	
*================================================================  
*-财务资料  1990-2019 资产负债表 利润表 现金流量表直接法/间接法
*-数据来源:[中国上市公司公司研究系列数据库/财务报表]
*================================================================  
   
   *-负债表
     local ff "FS_Combas"
     import delimited "`ff'.csv", case(preserve) encoding(UTF-16LE) clear 
     gen str2 month = substr(Accper,6,7)	//产生月份
     keep if Typrep == "A"				    //仅保留合并报表
     keep if month == "12"                  //仅保留年度数据
     save "$Out\FS_Combas.dta", replace
  
   *-利润表
	 local ff "FS_Comins"
     import delimited "`ff'.csv", case(preserve) encoding(UTF-16LE) clear 
     gen str2 month = substr(Accper,6,7)	//产生月份
     keep if Typrep == "A"				    //仅保留合并报表
     keep if month == "12"                  //仅保留年度数据
     save "$Out\FS_Comins.dta", replace
    
   *-现金流量表（直接法）
	 local ff "FS_Comscfd"
     import delimited "`ff'.csv", case(preserve) encoding(UTF-16LE) clear 
     gen str2 month = substr(Accper,6,7)	//产生月份
     keep if Typrep == "A"				    //仅保留合并报表
     keep if month == "12"                  //仅保留年度数据
     save "$Out\FS_Comscfd.dta", replace
	
   *-现金流量表（间接法法）
	 local ff "FS_Comscfi"
     import delimited "`ff'.csv", case(preserve) encoding(UTF-16LE) clear 
     gen str2 month = substr(Accper,6,7)	//产生月份
     keep if Typrep == "A"				    //仅保留合并报表
     keep if month == "12"                  //仅保留年度数据
     save "$Out\FS_Comscfi.dta", replace
	
   *-Note：为了不改变原始数据文件名称，这里并没有使用循环语句
   *       也可以将四张报表分别命名为1、2、3、4，然后使用forvalues循环语句
	
   *-合并四张报表
	 use "$Out\FS_Combas.dta", clear
	 merge 1:1 Stkcd Accper using "$Out\FS_Comins.dta", nogen
	 merge 1:1 Stkcd Accper using "$Out\FS_Comscfd.dta", nogen
	 merge 1:1 Stkcd Accper using "$Out\FS_Comscfi.dta", nogen
	  
   *-添加变量标签
       do "$do\Finsheet_label.do"  //这个dofile中存储了变量标签
  
    *-仅保留年度资料
        *keep if strpos(Accper, "-12-")!=0     //数据合并前已经处理
		gen year = real(substr(Accper,1,4))
		drop Accper Typrep
		*drop if year==2019              //2019年的数据可能不全（可选项）
		order Stkcd year
		xtset Stkcd year

	*-保存资料
	  save "$Out\FSsheet.dta", replace   //三大财务报表合并资料
	  
	  *use "$Out\FSsheet.dta", clear

*================================================================  
*-财务指标  1990-2019 偿债能力 发展能力 经营能力 比率结构 等等
*-数据来源:[中国上市公司公司研究系列数据库/财务指标分析]
*================================================================	  
	  
    *-偿债能力\披露财务指标\比率结构\经营能力\盈利能力\现金流量分析
	*-\风险水平\发展能力\每股指标\相对价值指标\股利分配
    foreach s in FI_T1 FI_T2 FI_T3 FI_T4 FI_T5 ///
			FI_T6 FI_T7 FI_T8 FI_T9 FI_T10 FI_T11{
	  import delimited "`s'.csv", case(preserve) encoding(UTF-16LE) clear
	  gen date = date(Accper, "YMD")
	  cap keep if Typrep== "A"               //仅保留合并报表
	  gen str2 month = substr(Accper,6,7)	 //产生月份
      cap keep if month == "12"				 //仅保留年度报表
	  sort Stkcd date
	  save "$Out/`s'_tmp.dta", replace
	}
  
  *-合并上述资料
    use "$Out\FI_T1_tmp.dta", clear
    foreach s in FI_T1 FI_T2 FI_T3 FI_T4 FI_T5 ///
				FI_T6 FI_T7 FI_T8 FI_T9 FI_T10 FI_T11{
	  merge 1:1 Stkcd date using "$Out/`s'_tmp.dta", nogen
	}	
	*keep if strpos(Accper, "-12-")!=0 		 //已经在合并前处理
	gen year = year(date)
	compress
	xtset Stkcd year
  
  *-添加变量标签
    do "$do\Fin_ratio_label.do"  //这个dofile中存储了变量标签
  
    drop Indcd  // 一定要执行，因为前面用过，否则合并会出问题
    
  *-保存财务比率合并后的资料	
	save "$Out\Fin_ratios.dta", replace  //财务比率合并后数据
  
*============================================ 
*-民营上市公司
*-数据来源:[公司研究系列数据库/民营上市公司]
*============================================	  
	  	  
	local ff "PRI_Basic"
    import delimited "`ff'.csv", case(preserve) encoding(UTF-16LE) clear  
	gen Priyear = real(substr(Pridt,1,4))
    gen year = year(date(Reptdt, "YMD"))
	
	*-添加变量标签
	do "$do\PRI_Basic_label.do"  //这个dofile中存储了变量标签
    
	drop Reptdt Pridt Listdt
    * destring Stkcd Primed Prisg, replace    // csv格式导入，无需转换
  
  *-是否始终为民营公司
    bysort Stkcd: egen sd = sd(Prisg)         
	gen priYESall = (sd==0)                   // 若性质没变，标准差为0
	label var priYESall "是否始终为民营企业"
	label define priYESall 1 "是" 0 "否"
	label value  priYESall priYESall 
  
  *-添加新变量标签
	label var	Stkcd		"证券代码"
	label var	Stknme		"证券简称"
	label var	Primed		"民营化方式"
	label var	Prisg		"民营化标志"
	label var	Priyear		"民营转化年份"
  
  *-数字-文字对应表	
    label define Primed 1 "直接上市" 2 "间接上市"  
    label value  Primed  primed   
    label define Prisg   1 "民营" 0 "非民营"  
    label value Prisg  Prisg   
  
  *-保存数据
    xtset Stkcd year
    save "$Out\PRI_Basic.dta", replace
	
*============================================ 
*-公司治理
*-数据来源:[公司研究系列数据库/治理结构/]
*============================================

  *-----------------	  
  *-公司基本情况文件
  *-数据来源:[公司研究系列数据库/治理结构/基础数据/]
  *-----------------
  local ff "CG_Co"
	import delimited "`ff'.csv", case(preserve) encoding(UTF-16LE) clear 
  
	*-添加变量标签
	 label var        Stkcd              "证券代码"  
     label var       Stknme          "证券中文简称"  
     label var    Stknme_en          "证券英文简称"  
     label var      Stktype              "股票类型"  
     label var         Crcd         "A/B/H股交叉码"  
     label var     Repttype              "公告类型"  
     label var       Reptdt          "统计截止日期"  
     label var        Conme              "公司名称"  
     label var      Cochsnm          "公司中文简称"  
     label var       Conmee          "公司英文名称"  
     label var      Coensnm          "公司英文简称"  
     label var     Nnindnme            " 行业名称C"  
     label var      Nnindcd             "行业代码C"  
     label var      Nindnme             "行业名称B"  
     label var       Nindcd             "行业代码B"  
     label var       Indnme             "行业名称A"  
     label var        Indcd             "行业代码A"  
     label var     Busscope              "经营范围"  
     label var      Cohisty              "公司沿革"  
     label var       Regcap              "注册资本"  
    
	rename Crcd CrcdABH
	drop Reptdt
	*destring Stkcd, replace      // csv格式导入，无需转换
	save "$Out\CG_Co.dta", replace  
  
  *-----------------
  *-治理综合信息文件
  *-数据来源:[公司研究系列数据库/治理结构/基础数据/]
  *-----------------
    forvalues i=1/5 {
    import delimited CG_Ybasic`i'.csv, ///
				case(preserve) encoding(UTF-16LE) clear
    save "$Out\CG_Ybasic`i'.dta", replace
  }
   *-Note：综合治理信息最多一次下载五年，五个文件命分别1-5
  
   *-合并上述资料
    use "$Out\CG_Ybasic1.dta", clear
    forvalues i=2/5 {
    append using "$Out\CG_Ybasic`i'.dta"
  }
  
    gen year = year(date(Reptdt, "YMD"))
    *-添加变量标签
    do "$do\CG_Ybasic_label.do"  //这个dofile中存储了变量标签
	*-数字-文字对应表
	replace Y0301b = Y0301b-1
	  label define Y0301b 0 "未变化" 1 "有变化"
	  label value  Y0301b Y0301b 
	replace Y1001b=0 if Y1001b==2
	  label define Y1001b   1 "同一人" 0 "不同一人"  
      label value  Y1001b  Y1001b
  
    save "$Out\CG_Ybasic.dta", replace

  *-------------
  *-股本结构文件
  *-数据来源:[公司研究系列数据库/治理结构/股东股本/]
  *-------------
    local ff "CG_Capchg"
      import delimited "`ff'.csv", case(preserve) encoding(UTF-16LE) clear 
	  gen year = year(date(Reptdt, "YMD"))
	  *destring stkcd, replace    //csv格式导入，无需转换
	*-添加变量标签
	  label var	Stkcd		"证券代码"
	  label var	Reptdt		"统计截止日期"
	  label var	Nshrttl		"总股数"
	  label var	Nshrnn		"未流通股份"
	  label var	Nshrstt		"其中:国有股股数"
      label var	Nshrlpd		"其中:境内发起人法人股股数"
	  label var	Nshrlpf		"其中:境外发起人法人股股数"
      label var	Nshrlpn		"其中:募集法人股股数"
	  label var	Nshremp		"其中:内部职工股股数"
	  label var	Nshrmf		"其中:基金配售股数"
	  label var	Nshrrot		"其中:转配股股数"
	  label var	Nshrprf		"其中:优先股股数"
	  label var	Nshrunl		"其中:流通配送股尚未流通股数"
	  label var	Nshrsms		"其中:监管层持股数"
      label var	Nshrglea	"其中:一般法人配售数"
	  label var	Nshrsina	"其中:战略投资者配售数"
	  label var	Nshrlpo		"其中:其他股数"
	  label var	Nshrn		"已流通股份"
	  label var	Nshra		"其中:A股流通股数"
	  label var	Nshrb		"其中:B股流通股数"
	  label var	Nshrh		"其中:H股流通股数"
	  label var	Nshroft		"其中:其它境外流通股"
	  
	  save "$Out\CG_Capchg.dta", replace    
  
    *-------------
    *-十大股东文件
    *-数据来源:[公司研究系列数据库/治理结构/股东股本/]
    *-------------
  
  *-导入资料
  local ff "CG_Sharehold"
      import delimited "`ff'.csv", case(preserve) encoding(UTF-16LE) clear 
	gen year = year(date(Reptdt, "YMD"))
	gen str2 month = substr(Reptdt,6,7)	   //产生月份
    keep if month == "12"                  //仅保留年度数据
	drop Reptdt
	sort Stkcd year S0501b
	drop in 1                      // 这里可能是导入的数据有些问题
	destring Stkcd, replace        // 删除上述行才能保证 Stkcd 转化成功
	compress
	
	save "$Out\CG_Sharehold.dta", replace //年度资料，每年十个股东，以备后用
	
	*-汇总成年度资料
	  *-top1
	    gen top1 = S0301b if S0501b==1
	    *bysort stkcd year: egen top1 = min(top1_miss)
		label var top1 "第一大股东持股比例(%)"
	  *-top1_minus_2
	    sort Stkcd year S0501b
		bysort Stkcd year: gen top1_minus_2 = S0301b[1]-S0301b[2]
		replace top1_minus_2=0 if top1_minus_2==.
		label var top1_minus_2 "第一与第二大股东持股比例之差(%)"
	  *-top5 HHI
	    * ssc install hhi5
	    hhi5 S0301b, by(Stkcd year) top(5) pre(top5)
		rename top5_S0301b top5_HHI
		label var top5_HHI "前五大股东持股比例集中度 HHI"
	  *-top10 HHI
	  	hhi5 S0301b, by(Stkcd year) top(10) pre(top10)
		rename top10_S0301b top10_HHI
		label var top10_HHI "前十大股东持股比例集中度 HHI"
	  *-删除重复值
	    keep if top1 !=.
		keep Stkcd year top*
	  
	  save "$Out\CG_ShareTop10.dta", replace 
		
    *-----------------
    *-三会基本信息文件
    *-数据来源:[公司研究系列数据库/治理结构/会议情况/]
    *-----------------
	local ff "CG_Agm"
      import delimited "`ff'.csv", case(preserve) encoding(UTF-16LE) clear
	  gen year = year(date(Reptdt, "YMD"))
	  *destring stkcd, replace      // csv格式导入，无需转换
	  xtset Stkcd year
	  label var	A0101b		"董事会会议次数"
	  label var	A0201b		"监事会会议次数"
	  label var	A0301b		"股东大会召开次数"
	  
	  save "$Out\CG_Agm.dta", replace 

*============================================ 
*-股东
*-数据来源:[公司研究系列数据库/股东/]
*============================================ 
 
    *---------------------
    *-十大股东股权集中文件
    *-数据来源:[公司研究系列数据库/股东/股权信息/]
    *---------------------
     *-导入资料
     local ff "HLD_CR"
	   import delimited "`ff'.csv", case(preserve) encoding(UTF-16LE) clear 
	  gen year = year(date(Reptdt, "YMD"))
	  gen str2 month = substr(Reptdt,6,7)	   //产生月份
      keep if month == "12"                  //仅保留年度数据
	  drop Reptdt month
	 *-添加变量标签
	  label var       Stkcd                  "证券代码"   
      label var      Shrcr1          "股权集中指标1(%)"  
      label var      Shrcr2          "股权集中指标2(%)"  
      label var      Shrcr3          "股权集中指标3(%)"  
      label var      Shrcr4          "股权集中指标4(%)"  
      label var        Shrz                     "Z指数"  
      label var        Shrs                  "S指数(%)"  
      label var      Shrhfd                     "H指数"  
      label var     Shrhfd3          "Herfindahl_3指数"  
      label var     Shrhfd5          "Herfindahl_5指数"  
      label var    Shrhfd10         "Herfindahl_10指数"  
	  
	  order Stkcd year
	  compress
	  save "$Out\HLD_CR.dta", replace	  
	  
*============================================ 
*-股票市场
*-数据来源:[股票市场系列数据库/股票市场交易/]
*============================================ 

    *-----------------
    *-年个股回报率文件
    *-数据来源:[股票市场系列数据库/股票市场交易/个股交易数据]
    *----------------- 
     *-导入资料
     local ff "TRD_Year"
	   import delimited "`ff'.csv", case(preserve) encoding(UTF-16LE) clear 
	  * destring Trdynt, gen(year)   //csv格式导入，无需转换
	 *-添加变量标签
	  label var         Stkcd                                "证券代码"  
      label var        Trdynt                                "交易年份"  
      label var         Opndt                              "年开盘日期"  
      label var       Yopnprc                                "年开盘价"  
      label var         Clsdt                              "年收盘日期"  
      label var       Yclsprc                                "年收盘价"  
      label var      Ynshrtrd                          "年个股交易股数"  
      label var      Ynvaltrd                          "年个股交易金额"  
      label var       Ysmvosd                          "年个股流通市值"  
      label var       Ysmvttl                            "年个股总市值"  
      label var       Ndaytrd                              "年交易天数"  
      label var        Yretwd        "考虑现金红利再投资的年个股回报率"  
      label var        Yretnd      "不考虑现金红利再投资的年个股回报率"  
      label var    Yarkettype                                "市场类型"  
      label var      Capchgdt                        "最新股本变动日期"  
      label var    Ahshrtrd_Y                          "年盘后成交总量"  
      label var    Ahvaltrd_Y                          "年盘后成交总额"  

	  rename Trdynt year
	  save "$Out\TRD_year.dta", replace
	  

*===============
*-合并上述资料
*===============

   use "$Out\TRD_Co.dta", clear   //公司基本信息资料汇总
   merge 1:m Stkcd      using "$Out\FSsheet.dta", nogen       //三大财务报表合并资料
   merge 1:1 Stkcd year using "$Out\Fin_ratios.dta", nogen    //财务比率合并后数据
   merge 1:1 Stkcd year using "$Out\PRI_Basic.dta", nogen     //民营化指标
   merge m:1 Stkcd      using "$Out\CG_Co.dta", nogen         //注册资本\ABH交叉码
   merge 1:1 Stkcd year using "$Out\CG_Ybasic.dta", nogen     //公司治理基本信息
   merge 1:1 Stkcd year using "$Out\CG_Capchg.dta", nogen     //股本股东
   merge 1:1 Stkcd year using "$Out\CG_ShareTop10.dta", nogen //十大股东
   merge 1:1 Stkcd year using "$Out\CG_Agm.dta", nogen        //股东会议情况
   merge 1:1 Stkcd year using "$Out\HLD_CR.dta", nogen        //十大股东股权集中
   merge 1:1 Stkcd year using "$Out\TRD_year.dta", nogen      //年个股回报率	
	
  *-处理缺漏值   
   drop if year==2020|year==.
   *drop if year==2019            //2019年的数据可能不全（可选项）
   drop sd date
   mvdecode _all, mv(-9666)
   
   save "$path\GTA2019_orig.dta", replace
  

  * use "$path\GTA2019_orig.dta", clear

   	  
*===================== 
*-定义基本财务变量
*-参考来源：连玉君老师
*===================== 

  gen Arlion_def1 = .   //  空一行
  label var Arlion_def1 "--------------------------------------------------------"
  gen Arlion_def2 = .   //再空一行
  label var Arlion_def2 "-----------------以下变量为Arlion自行定义---------------"
  gen Arlion_def3 = .   //再空一行
  label var Arlion_def3 "--------------------------------------------------------"
  
  clonevar  TA = A001000000   // 总资产
  clonevar  TD = A002000000   // 总负债
  clonevar  Sale= B001101000   // 营业收入
  clonevar  tl = 	F011201A  // 资产负债率 = 负债合计/资产总计
  gen sl   = A002100000/TA   // 流动负债合计/总资产
  gen ll   = A002206000/TA    // 长期负债率 = 长期负债合计/总资产
  gen bankd= (A002101000+A002201000)/TD     // 短期借款+长期借款/总负债
  gen banklev = (A002101000+A002201000)/TA  // 短期借款+长期借款/总资产 
  gen tang = (A001212000+A001123000)/TA     // (固定资产净额+存货净额)/总资产  
  clonevar  itang= 	F030901A  // 无形资产比率 = 无形资产净额/资产总计
  gen liqui= ((A001100000-A002100000)-A001101000)/TA 
           /*(营运资本-货币资金)/总资产，营运资本=流动资产-流动负债*/
  clonevar  Finlev = F031701A // 金融负债比率 = (非流动D+短期借款+一年内到期的非流动D+交易性金融D+衍生金融D)/(D合
  clonevar  cr = 	F030101A  // 流动资产比率 = 流动资产合计/资产总计
  gen       cflow= C001000000/TA   // 经营活动产生的现金流净额/总资产
  
  replace A001107000=0 if A001107000==. //交易性金融资产
  gen cash = (A001101000 +A001107000)/A001000000 //(货币资金+交易性金融资产)/总资产
  *clonevar  cash = 	F030201a  // 现金资产比率 = 期末现金及现金等价物余额/资产总计
  gen       invt = C002006000/TA   // 购建固定资产、无形资产和其他长期资产支付的现金/总资产
  clonevar  arnr = 	F030301A  // 应收类资产比率 = (应收票据净额+应收账款净额)/(资产总计)

  clonevar  tagr = 	F080601A  // 总资产增长率A = (资产总计本期期末值-资产总计本期期初值)/(资产总计本期期初值)
  clonevar  tobin= 	F100901A  // 托宾Q值A = 市值A/资产总计
  clonevar  mbratio=F101001A  // 账面市值比A = 资产总计/市值A

  clonevar  roa = F050201B   //总资产净利润率(ROA)A = 净利润/总资产余额
  clonevar  roe = F050501B   //净资产收益率A = 净利润/股东权益余额
  
  clonevar  depamo = F061201B   //折旧摊销 = (固定资产折旧、油气资产折耗、生产性生物资产折旧+无形资产摊销+长期待摊

  gen size = ln(TA)             // ln(总资产)
  gen lnSale= ln(Sale)          // ln(营业收入)   
  
  gen age = year-Listyr 
  gen lnage = ln(age+1)   // age = year-IPO_year
  
  
  
 *-变量标签
   label var cflow   "现金流量=经营活动产生的现金流净额/总资产"
   label var cash    "现金持有量=(货币资金+交易性金融资产)/总资产"
   label var invt    "投资支出率=购建固定资产、无形资产和其他长期资产支付的现金/总资产"
   label var tl      "总负债率=负债合计/总资产"
   label var sl      "短期负债率=流动负债合计/总资产"
   label var ll      "长期负债率=长期负债合计/总资产"
   *label var llr     "债务期限结构_长债比率=长期负债/总负债"
   *label var slr     "债务期限结构_短债比率=流动负债/总负债"
   label var bankd   "银行借款(短期借款+长期借款)/总负债"
   label var banklev "银行借款(短期借款+长期借款)/总资产"
   label var tang    "资产结构=(固定资产净额+存货净额)/总资产"
   label var itang   "无形资产比重=无形资产净额/总资产"
   label var liqui   "现金替代物=(流动资产-流动负债-货币资金)/总资产"
   label var size    "公司规模=ln(总资产)"
   label var age     "Year-上市年份"
   label var lnage   "ln(Year-上市年份)"
   *label var tshr_r  "流通股比例=流通股/总股本"
   
  
   order Stkcd year Stknme Typrep     
   drop Accper *dt Statyear
   compress
   rename Stkcd id
   xtset id year
   
   save "$path\GTA2019.dta", replace      //最终保存的数据文件
   
   
   *use "$path\GTA2019.dta", clear
   *des                                   //查看数据 1990-2019中国上市公司数据

   
*========================== 
*-补充国泰安数据整理小知识
*-来源：连玉君老师
*==========================  

    *-----------------
    *-增加变量标签通用程序
    *-程序来源:连玉君
    *----------------- 
  /*-使用方法：
   [1]将GTA提供的 filename[DES][txt].txt 
      变量列表文件贴入 Stata data editor；首行为数据
   [2]保存为 filename_DES.dta
   [3]执行命令 GTAlabel filename
   [4]程序自己动将提取标签的文件存储为 label_filename.dta
   [5]随后使用 append 命令把多个存储标签信息的文件合并起来即可（可选项）
   [6]执行命令 list, clean noobs noheader 将添加变量标签呈现出来
   [7]将呈现添加变量标签的命令存入.do文件
   [8]执行do文件添加变量标签
   
   * GTAlabel 程序如下（Alion）  // 略有改写，去掉了一行变量小写的语句
   program define GTAlabel
     version 13
	 args s  // "label_CG_Agm_DES" 填入 "CG_Agm"
	 use `s'_DES.dta, clear
     split var1, p([ ]) gen(s)
     gen str20 v1 = "   label var "
     replace s2 = `"""' + s2 + `"""'
     gen a1 = " "
     gen a2 = a1
     order a1 v1 s1 a2 s2
     keep a1 v1 s1 a2 s2
     save label_`s'.dta, replace
   end 
   */ 
   *--------------------------------------------------------------
   
    *-----------------
    *-数字-文字对应表
    *-参考来源:连玉君
    *-----------------  

  *-从 GTA 的说明文件中提取定义[数字-文字对应表]的代码	  
	local s "Markettype [市场类型] 1=上海A，2=上海B，4=深圳A，8=深圳B，16=创业板"
	clear
	set obs 2
	gen str var1 = "`s'"
    split var1, p([ ]) gen(s)
    drop s2
    replace s1 = lower(s1)
    replace s3 = subinstr(s3, "=", `" ""',.)
    replace s3 = subinstr(s3, "，", `"" "',.)
    local v1 = s1[1]
	replace s3 = "label define " + "`v1' " + s3 + `"""' in 1
	replace s3 = "label value " + "`v1' " + "`v1'" in 2
	format s3 %-20s
	list s3, clean noobs noheader
  *--------------------------------------------------------------  
 

 
