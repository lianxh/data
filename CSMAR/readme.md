CSMAR 数据库合并过程。

- 按照 GTA2019.do 文档中的数据格式 (**.txt**，**.xlsx** 等)，从 CSMAR 中下载原始数据，进而按照 GTA2019.do 中的名称保存文件
- 存放位置可以自行修改，不过，为了节省时间，最好与 GTA2019.do 中的路径保持一致
- 使用过程中，有任何问题，可以发邮件至 <StataChina@163.com>

> Shared by [连享会](https://www.lianxh.cn)